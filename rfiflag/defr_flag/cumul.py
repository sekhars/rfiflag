"""
Worker functions for the rfiflag.defr_flag function, so I don't clutter
up the main rfiflag python file.
"""

import numpy as np
from rfiflag.lib import defringe, flagger, antennas, strings
from rfiflag.lib.cstats import sigmaclip, std, mean


def defr_and_write_base(ffile, fitspars, chunk, fouriersigma, max_baseline):
    """
    Writes out the sliding window statistics to
    binary files, per baseline.

    Inputs:
        ffile           FITS file object, astropy object
        fitspars        Namedtuple of fits parameters
        chunk           Chunk size to use to read in FITS file
        fouriersigma    Sigma above which to clip in the fourier plane
        perc            Percentage of points within window to use for stats

    Returns:
        None
    """

    bname_ri = ['tmpri%03d' % (bb + 1) for bb in range(max_baseline)]
    bname_amp = ['tmpamp%03d' % (bb + 1) for bb in range(max_baseline)]

    bfptr_ri = [open(bb, 'ab') for bb in bname_ri]
    bfptr_amp = [open(bb, 'ab') for bb in bname_amp]

    for group in range(0, fitspars.gcount, chunk):
        baselines = ffile[0].data[group:group + chunk].par('BASELINE')
        try:  # See if baselines need to be converted to sequential values
            baselines = antennas.baseline_aips_sequential(baselines)
        except:
            pass

        imgdat = ffile[0].data[group:group + chunk].data
        imgdat = imgdat.byteswap().newbyteorder()
        imgdat = np.squeeze(imgdat).astype(np.float64)

        strings.clearline()
        for bb in range(0, max_baseline):
            if bb % 10 == 0:
                print(
                    "Processing group {}/{} Base {}".format(
                        group, fitspars.gcount, bb + 1),
                    end="\r")

            idx = np.where(baselines == bb + 1)
            basedat = imgdat[idx]

            if basedat.size == 0:
                continue

            defr_dat = defringe.defr_base(basedat, fouriersigma, fitspars.npol)

            if defr_dat is None:
                continue

            for pp in range(fitspars.npol):
                redat = np.where(defr_dat[..., pp, 2], defr_dat[..., pp, 0], 0)
                imdat = np.where(defr_dat[..., pp, 2], defr_dat[..., pp, 1], 0)

                ampdat = np.sqrt(redat**2 + imdat**2)

                bfptr_ri[bb].write(redat[redat != 0])
                bfptr_ri[bb].write(imdat[imdat != 0])

                bfptr_amp[bb].write(ampdat[ampdat != 0])

    for bb in bfptr_ri:
        bb.close()

    for bb in bfptr_amp:
        bb.close()


def calc_cutoffs(max_baseline, nsigma):
    """
    Calculate the cutoffs per baseline for standard deviation, deviation
    and range separately.

    Inputs:
        max_baseline    Largest baseline present in FITS file, int
        nsigma          Sigma above which to cutoff, float
    Returns:
        cutoffs_std     Cutoffs per baseline for standard deviation
        cutoffs_dvn     Cutoffs per baseline for deviation
        cutoffs_rng     Cutoffs per baseline for range
    """

    cutoffs_ri = np.zeros([max_baseline, 2])
    cutoffs_amp = np.zeros([max_baseline, 2])

    bname_ri = ['tmpri%03d' % (bb + 1) for bb in range(max_baseline)]
    bname_amp = ['tmpamp%03d' % (bb + 1) for bb in range(max_baseline)]

    strings.clearline()
    for bb in range(max_baseline):
        if bb % 10 == 0:
            print(
                "Calculating cutoffs baseline {}/{}".format(
                    bb + 1, max_baseline),
                end="\r")

        try:
            ridat = np.fromfile(bname_ri[bb], count=-1)
            if ridat.size <= 5:
                continue

            ridat = sigmaclip(ridat, ridat.size)
            #ridat = sigmaclip(ridat, low=3, high=3)[0]
            size = ridat.size

            if size <= 5:
                continue

            mn = mean(ridat, size)
            st = std(ridat, size)

            cutoffs_ri[bb, 0] = mn - nsigma * st
            cutoffs_ri[bb, 1] = mn + nsigma * st
        except FileNotFoundError:  # File isn't there, so keep going
            pass

        try:
            ampdat = np.fromfile(bname_amp[bb], count=-1)

            if ampdat.size <= 5:
                continue

            ampdat = sigmaclip(ampdat, ampdat.size)
            #ampdat = sigmaclip(ampdat, low=3, high=3)[0]
            size = ampdat.size

            if size <= 5:
                continue

            mn = mean(ampdat, size)
            st = std(ampdat, ampdat.size)

            cutoffs_amp[bb, 0] = mn - nsigma * st
            cutoffs_amp[bb, 1] = mn + nsigma * st

        except FileNotFoundError:  # File isn't there, so keep going
            pass

    return cutoffs_ri, cutoffs_amp


def apply_cutoffs(ffile, fitspars, chunk, cutoffs_ri, cutoffs_amp,
                  max_baseline, fouriersigma):
    """
    Apply cutoffs from sliding window statistics to the FITS file, flagging
    points that are statistical outliers.
    """

    for group in range(0, fitspars.gcount, chunk):
        baselines = ffile[0].data[group:group + chunk].par('BASELINE')
        try:  # See if baselines need to be converted to sequential values
            baselines = antennas.baseline_aips_sequential(baselines)
        except:
            pass

        imgdat = ffile[0].data[group:group + chunk].data
        imgdat = imgdat.byteswap().newbyteorder()
        imgdat = np.squeeze(imgdat).astype(np.float64)

        strings.clearline()
        for bb in range(0, max_baseline):
            if bb % 10 == 0:
                print(
                    "Appying flags group {}/{} Base {}".format(
                        group, fitspars.gcount, bb + 1),
                    end="\r")

            idx = np.where(baselines == bb + 1)
            basedat = imgdat[idx]

            if basedat.size == 0:
                continue

            defr_dat = defringe.defr_base(basedat, fouriersigma, fitspars.npol)

            if defr_dat is None:
                continue

            for pp in range(fitspars.npol):
                redat = np.where(defr_dat[..., pp, 2], defr_dat[..., pp, 0], 0)
                imdat = np.where(defr_dat[..., pp, 2], defr_dat[..., pp, 1], 0)

                ampdat = np.sqrt(redat**2 + imdat**2)

                cond1 = (redat < cutoffs_ri[bb, 0]) | (redat >
                                                      cutoffs_ri[bb, 1])
                cond2 = (ampdat < cutoffs_amp[bb, 0]) | (ampdat >
                                                        cutoffs_amp[bb, 1])

                cond = cond1 | cond2

                basedat[..., pp, 2] = np.where(cond, 0, basedat[..., pp, 2])

            imgdat[idx] = basedat

        imgdat = imgdat.byteswap().newbyteorder().astype(np.float32)
        ffile[0].data[group:group + chunk].base['DATA'] = imgdat[:, None, None,
                                                                 None, ...]
        ffile.flush()
