"""
Worker functions for the rfiflag.rflag function, so I don't clutter
up the main rfiflag python file.
"""

import numpy as np
import numba as nb
from numba import prange

from rfiflag.lib import cstats


@nb.jit(nopython=True, nogil=True, cache=True)
def mad(inparr):
    """
    Calculate the median absolute deviation given an input array.
    """

    med = np.median(inparr)

    mad = np.median(np.abs(inparr - med))

    return mad


@nb.jit(nopython=True, nogil=True, parallel=True)
def sliding_time(inpdat, tsigma, window):
    """
    Run a sliding window in the time direction, and use a sliding window
    to flag

    Inputs:
        inpdat      The input data of shape [ngroup, nchan, npol]
        tsigma      Sigma cutoff factor in the time direction
        window      Size of the sliding window in pixels

    Returns:
        inpdat      The input data with the flags applied
    """

    ntime = inpdat.shape[0]
    nchan = inpdat.shape[1]
    npol = inpdat.shape[2]

    if ntime <= window:
        return inpdat

    mads = np.zeros(2)
    meds = np.zeros(2)

    winrms = np.zeros((ntime, npol))

    halfs = window // 2

    for cc in prange(nchan):
        for tt in range(halfs, ntime - halfs):
            for pp in range(npol):
                winrms[tt, pp] = np.std(inpdat[tt - halfs:tt + halfs, cc, pp])

        for pp in range(npol):
            meds[pp] = np.median(winrms[:, pp])
            mads[pp] = mad(winrms[:, pp])

        for tt in range(halfs, ntime - halfs):
            for pp in range(npol):
                if winrms[tt, pp] > tsigma * (meds[pp] + mads[pp]):
                    inpdat[tt, cc, pp] = 0

    return inpdat


@nb.jit(nopython=True, nogil=True, parallel=True)
def flag_chan(inpdat, csigma):
    """
    Flag deviant channels which have a local RMS above csigma

    Inputs:
        inpdat      The input data of shape [ngroup, nchan, npol, 3]
        csigma      Sigma cutoff factor in the channel direction

    Returns:
        inpdat      Input data with the flags applied
    """

    ntime = inpdat.shape[0]
    nchan = inpdat.shape[1]
    npol = inpdat.shape[2]

    if ntime <= 0:
        return inpdat

    for tt in prange(ntime):
        for pp in range(npol):
            chanmad = mad(inpdat[tt, :, pp])
            chandev = np.abs(inpdat[tt, :, pp] - chanmad)

            for cc in range(nchan):
                if chandev[cc] > csigma * chanmad:
                    inpdat[tt, cc, pp] = 0

    return inpdat


def flagger(inpdat, tsigma, csigma, fsigma, twin, npol):
    """
    After defringing the TC plane, run the RFlag algorithm given the
    respective sigma cutoffs.

    Inputs:
        inpdat      Array of shape [ngroup, nchan, npol, 3]
        tsigma      Sigma cutoff factor in the time direction
        csigma      Sigma cutoff factor in the channel direction
        twin        Size of the sliding window in the time direction
        npol        Number of polarizations in the data

    Returns:
        inpdat      The input data with the flags applied
    """

    inpdat = sliding_time(inpdat, tsigma, twin)
    inpdat = flag_chan(inpdat, csigma)

    return inpdat
