"""
Worker functions for the rfiflag.defr_flag function, so I don't clutter
up the main rfiflag python file.
"""

import numpy as np
from rfiflag.lib import defringe, flagger, antennas, strings
from rfiflag.lib.cstats import sigmaclip, std, mean

def calc_and_write_base_stats(ffile, fitspars, chunk, fouriersigma, halfc,
                              halft, perc):
    """
    Writes out the sliding window statistics to
    binary files, per baseline.

    Inputs:
        ffile           FITS file object, astropy object
        fitspars        Namedtuple of fits parameters
        chunk           Chunk size to use to read in FITS file
        fouriersigma    Sigma above which to clip in the fourier plane
        halfc           Half size of sliding window in channel direction
        halft           Half size of sliding window in time direction
        perc            Percentage of points within window to use for stats

    Returns:
        None
    """


    max_baseline = 0
    for group in range(0, fitspars.gcount, chunk):

        baselines = ffile[0].data[group:group + chunk].par('BASELINE')
        try:  # See if baselines need to be converted to sequential values
            baselines = antennas.baseline_aips_sequential(baselines)
        except:
            pass

        maxb = int(baselines.max())
        bnamestd = ['tmpstd%03d' % (bb + 1) for bb in range(maxb)]
        bnamedvn = ['tmpdvn%03d' % (bb + 1) for bb in range(maxb)]
        bnamerng = ['tmprng%03d' % (bb + 1) for bb in range(maxb)]
        #bnamemed = ['tmpmed%03d' % (bb + 1) for bb in range(maxb)]

        max_baseline = np.maximum(max_baseline, int(baselines.max()))

        imgdat = ffile[0].data[group:group + chunk].data
        imgdat = imgdat.byteswap().newbyteorder()
        imgdat = np.squeeze(imgdat).astype(np.float64)

        strings.clearline()
        for bb in range(0, maxb):
            if bb % 10 == 0:
                print(
                    "Processing group {}/{} Base {}".format(
                        group, fitspars.gcount, bb + 1),
                    end="\r")

            idx = np.where(baselines == bb + 1)
            basedat = imgdat[idx]

            if basedat.size == 0:
                continue

            defr_dat = defringe.defr_base(basedat, fouriersigma, fitspars.npol)

            if defr_dat is None:
                continue

            for pp in range(fitspars.npol):
                redat = np.where(defr_dat[..., pp, 2], defr_dat[..., pp, 0], 0)
                imdat = np.where(defr_dat[..., pp, 2], defr_dat[..., pp, 1], 0)

                # Too few points to calculate stats
                if np.count_nonzero(redat) <= 10 and np.count_nonzero(
                        imdat) <= 10:
                    continue

                stdre, rngre, dvnre= flagger.surface_stats(
                    redat, halfc, halft, perc)
                stdim, rngim, dvnim = flagger.surface_stats(
                    imdat, halfc, halft, perc)

                with open(bnamestd[bb], mode='ab') as fptr:
                    fptr.write(stdre.ravel())
                    fptr.write(stdim.ravel())

                with open(bnamedvn[bb], mode='ab') as fptr:
                    fptr.write(dvnre.ravel())
                    fptr.write(dvnim.ravel())

                with open(bnamerng[bb], mode='ab') as fptr:
                    fptr.write(rngre.ravel())
                    fptr.write(rngim.ravel())

                #with open(bnamemed[bb], mode='ab') as fptr:
                #    fptr.write(medre.ravel())
                #    fptr.write(medim.ravel())

    return max_baseline


def calc_cutoffs(max_baseline, nsigma):
    """
    Calculate the cutoffs per baseline for standard deviation, deviation
    and range separately.

    Inputs:
        max_baseline    Largest baseline present in FITS file, int
        nsigma          Sigma above which to cutoff, float
    Returns:
        cutoffs_std     Cutoffs per baseline for standard deviation
        cutoffs_dvn     Cutoffs per baseline for deviation
        cutoffs_rng     Cutoffs per baseline for range
    """

    cutoffs_std = np.zeros([max_baseline, 2])
    cutoffs_dvn = np.zeros([max_baseline, 2])
    cutoffs_rng = np.zeros([max_baseline, 2])
    #cutoffs_med = np.zeros([max_baseline, 2])

    bnamestd = ['tmpstd%03d' % (bb + 1) for bb in range(max_baseline)]
    bnamedvn = ['tmpdvn%03d' % (bb + 1) for bb in range(max_baseline)]
    bnamerng = ['tmprng%03d' % (bb + 1) for bb in range(max_baseline)]
    #bnamemed = ['tmpmed%03d' % (bb + 1) for bb in range(max_baseline)]

    strings.clearline()
    for bb in range(max_baseline):
        if bb % 10 == 0:
            print(
                "Calculating cutoffs baseline {}/{}".format(
                    bb + 1, max_baseline),
                end="\r")
        try:
            stddat = np.fromfile(bnamestd[bb], count=-1)
            stddat = sigmaclip(stddat, stddat.size)
            size = stddat.size
            cutoffs_std[bb, 0] = mean(stddat,
                                      size) - nsigma * std(stddat, size)
            cutoffs_std[bb, 1] = mean(stddat,
                                      size) + nsigma * std(stddat, size)
        except FileNotFoundError:  # File isn't there, so keep going
            pass

        try:
            dvndat = np.fromfile(bnamedvn[bb], count=-1)
            dvndat = sigmaclip(dvndat, dvndat.size)
            size = dvndat.size
            cutoffs_dvn[bb, 0] = mean(dvndat,
                                      size) - nsigma * std(dvndat, size)
            cutoffs_dvn[bb, 1] = mean(dvndat,
                                      size) + nsigma * std(dvndat, size)
        except FileNotFoundError:  # File isn't there, so keep going
            pass

        try:
            rngdat = np.fromfile(bnamerng[bb], count=-1)
            rngdat = sigmaclip(rngdat, rngdat.size)
            size = rngdat.size
            cutoffs_rng[bb, 0] = 0.
            cutoffs_rng[bb, 1] = mean(rngdat,
                                      size) + nsigma * std(rngdat, size)
        except FileNotFoundError:  # File isn't there, so keep going
            pass

        #try:
        #    meddat = np.fromfile(bnamemed[bb], count=-1)
        #    meddat = sigmaclip(meddat, meddat.size)
        #    size = meddat.size
        #    cutoffs_med[bb, 0] = mean(meddat,
        #                              size) - nsigma * std(meddat, size)
        #    cutoffs_med[bb, 1] = mean(meddat,
        #                              size) + nsigma * std(meddat, size)
        #except FileNotFoundError:  # File isn't there, so keep going
        #    pass
        #print("calc med")

    return cutoffs_std, cutoffs_dvn, cutoffs_rng


def apply_cutoffs(ffile, fitspars, chunk, cutoffs_std, cutoffs_dvn,
                  cutoffs_rng, max_baseline, fouriersigma, halfc,
                  halft, perc):
    """
    Apply cutoffs from sliding window statistics to the FITS file, flagging
    points that are statistical outliers.
    """

    bnamestd = ['tmpstd%03d' % (bb + 1) for bb in range(max_baseline)]
    bnamedvn = ['tmpdvn%03d' % (bb + 1) for bb in range(max_baseline)]
    bnamerng = ['tmprng%03d' % (bb + 1) for bb in range(max_baseline)]

    for group in range(0, fitspars.gcount, chunk):
        baselines = ffile[0].data[group:group + chunk].par('BASELINE')
        try:  # See if baselines need to be converted to sequential values
            baselines = antennas.baseline_aips_sequential(baselines)
        except:
            pass

        imgdat = ffile[0].data[group:group + chunk].data
        imgdat = imgdat.byteswap().newbyteorder()
        imgdat = np.squeeze(imgdat).astype(np.float64)

        strings.clearline()
        for bb in range(0, max_baseline):
            if bb % 10 == 0:
                print(
                    "Appying flags group {}/{} Base {}".format(
                        group, fitspars.gcount, bb + 1),
                    end="\r")

            idx = np.where(baselines == bb + 1)
            basedat = imgdat[idx]

            if basedat.size == 0:
                continue

            defr_dat = defringe.defr_base(basedat, fouriersigma, fitspars.npol)

            if defr_dat is None:
                continue

            for pp in range(fitspars.npol):
                redat = np.where(defr_dat[..., pp, 2], defr_dat[..., pp, 0], 0)
                imdat = np.where(defr_dat[..., pp, 2], defr_dat[..., pp, 1], 0)

                # Too few points to calculate stats
                if np.count_nonzero(redat) <= 10 and np.count_nonzero(
                        imdat) <= 10:
                    continue

                stdre, rngre, dvnre= flagger.surface_stats(
                    redat, halfc, halft, perc)
                stdim, rngim, dvnim = flagger.surface_stats(
                    imdat, halfc, halft, perc)

                cond = (stdre < cutoffs_std[bb, 0]) | (stdre >
                                                       cutoffs_std[bb, 1])
                cond = cond | (dvnre < cutoffs_dvn[bb, 0]) | (
                    dvnre > cutoffs_dvn[bb, 1])
                cond = cond | (rngre < cutoffs_rng[bb, 0]) | (
                    rngre > cutoffs_rng[bb, 1])
                #cond = cond | (medre < cutoffs_med[bb, 0]) | (
                #    medre > cutoffs_med[bb, 1])

                cond = cond | (stdim < cutoffs_std[bb, 0]) | (
                    stdim > cutoffs_std[bb, 1])
                cond = cond | (dvnim < cutoffs_dvn[bb, 0]) | (
                    dvnim > cutoffs_dvn[bb, 1])
                cond = cond | (rngim < cutoffs_rng[bb, 0]) | (
                    rngim > cutoffs_rng[bb, 1])
                #cond = cond | (medim < cutoffs_med[bb, 0]) | (
                #    medim > cutoffs_med[bb, 1])

                basedat[..., pp, 2] = np.where(cond, 0, basedat[..., pp, 2])

            imgdat[idx] = basedat

        imgdat = imgdat.byteswap().newbyteorder().astype(np.float32)
        ffile[0].data[group:group + chunk].base['DATA'] = imgdat[:, None, None,
                                                                 None, ...]
        ffile.flush()
