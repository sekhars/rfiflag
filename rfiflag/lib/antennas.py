"""
Module to hold functions pertaining to baselines or antennas, but not
necessarily to FITS I/O.
"""

import numpy as np

def baseline_aips_sequential(baseline, anttotal=30, doself=False):
    """
    Converts the baseline numbers from AIPS format (258, 259... etc.) to
    sequential numbers (1, 2, ...).

    Input:
        baseline    Baseline number to convert - integer or array-like object
        anttotal    Total number of antennas in the interferometer
        doself      Input baselines contain self-baselines, bool

    Returns:
        seq_base    The sequential baseline number
    """

    # Baseline is an array - Convert to ints
    if hasattr(baseline, '__len__'):
        baseline = np.array(baseline)
        baseline = baseline.astype(int)  # Make sure they are integers
    else:
        try:  # Try to coerce baseline to becoming an int
            baseline = int(baseline)
        except TypeError:
            raise TypeError("baseline variable must be an integer type")

    ant1     = baseline // 256
    ant2     = baseline % 256

    if (np.any(ant1) < 1):
        raise ValueError("Antenna number cannot be less than 1.")

    if np.any(np.greater(ant1, ant2)):
        raise ValueError('ant1 <= ant2 should always hold.')

    if np.any(ant1 > anttotal) or np.any(ant2 > anttotal):
        raise ValueError('Antenna numbers found to be greated than'
                'total number of antennas.')

    if doself:
        seqbase = (ant1 - 1) * (2*anttotal - ant1)//2 + ant2
    else:
        seqbase = (ant1-1)*(2*anttotal - ant1)//2 + ant2 - ant1

    return seqbase
