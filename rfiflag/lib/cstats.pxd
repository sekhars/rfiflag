# distutils: language = c++
cimport cython
from cython cimport floating

cpdef floating std(floating[:] inparr, int size) nogil
cpdef floating median(floating[:] inparr, int size) nogil
cpdef floating mean(floating[:] inparr, int size) nogil
cpdef floating pmean(floating[:] inparr, int size) nogil

cpdef floating[:] sigmaclip(floating[:] inparr, int size, floating nsigma=?)
cpdef double[:] histogram1d(floating[:] data, floating[:] limits,
                            floating delta, int bins)
