# distutils: language = c++
"""
Module to have some fast statistics functions, partially replacing numpy/scipy
functionality.
"""

from libc.stdio cimport printf
from libcpp.algorithm cimport sort
from libc.math cimport sqrt

from cython.parallel import prange

import numpy as np
cimport numpy as np


cpdef floating std(floating[:] inparr, int size) nogil:
    """
    Calculate the standard deviation of the input 1 dimensional
    array, which has to be C contiguous.

    Inputs:
        inparr    Input 1D array, of floating type
        size      Size of the in put array
    Returns:
        std       Standard deviation of array
    """

    # This assumes the data are all of similar order, and
    # scales the dataset by the first member for greater numerical stability.
    # See:  https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance

    cdef floating mean, mean2, K, diff, var
    cdef int count, ii

    count = 0
    mean = mean2 = 0

    K = inparr[0]
    for ii in range(size):
        count += 1
        diff = inparr[ii] - K
        mean += diff
        mean2 += diff * diff

    var = (mean2 - (mean*mean)/count)/(count-1)

    return sqrt(var)


cpdef floating median(floating[:] inparr, int size) nogil:
    """
    Calculate the median of the input 1 dimensional array.

    Inputs:
        inparr     Input 1D array, of floating type
        size       Size of the input array

    Returns:
        median     The median value of the array
    """

    cdef floating med
    cdef int ii

    # Use the C++ sorting function
    sort(&inparr[0], (&inparr[0]) + size - 1)

    if size % 2 == 0:
        med = 0.5 * (inparr[size/2] + inparr[size/2 - 1])
    else:
        med = inparr[size/2]

    return med


cpdef floating mean(floating[:] inparr, int size) nogil:
    """
    Calculates the mean of the input array.

    Inputs:
        inparr    The input array, floating
        size      Size of the in put array, int

    Returns:
        mean      The arithmetic mean of the input data
    """

    cdef floating mean, summ
    cdef int ii

    summ = 0
    for ii in range(size):
        summ += inparr[ii]

    summ /= size

    return summ


cpdef floating pmean(floating[:] inparr, int size) nogil:
    """
    Calculates the mean of the input array, but the mean
    is calculated in parallel using all the threads available.
    This could be significantly faster for larger datasets.

    Inputs:
        inparr    The input array, floating
        size      Size of the in put array, int

    Returns:
        mean      The arithmetic mean of the input data
    """

    cdef floating mean, summ
    cdef int ii

    summ = 0
    for ii in prange(size, schedule='dynamic'):
        summ += inparr[ii]

    summ /= size

    return summ

cpdef floating[:] sigmaclip(floating[:] inparr, int size, floating nsigma=3):
    """
    Iteratively sigma clip the input array, and return the clipped array.
    If niter is less than 0, the iterations are continued until the relative
    change in the standard deviation is less that ``delta``.

    Inputs:
        inparr    The input 1D array, floating
        size      The size of the input array, int
        nsigma    Sigma above which to clip the array, floating
    """

    cdef floating arrmean, stdev, oldstd
    cdef int indexl, indexr, prevsize, delta, ii

    # Sort so that cutoffs can be easily figured out on each side
    sort(&inparr[0], (&inparr[0]) + size - 1)

    indexl = 0
    indexr = size
    arrmean = mean(inparr[indexl:indexr], size)
    stdev = std(inparr[indexl:indexr], size)

    prevsize = indexr - indexl + 1
    delta = 1
    while delta:
        oldstd = stdev

        for ii in range(indexl, size):
            if inparr[ii] >= arrmean - nsigma*stdev:
                indexl = ii
                break

        for ii in range(indexr, 0, -1):
            if inparr[ii] <= arrmean + nsigma*stdev:
                indexr = ii
                break

        size = indexr - indexl + 1
        arrmean = mean(inparr[indexl:indexr], size)
        stdev = std(inparr[indexl:indexr], size)

        delta = prevsize - (indexr - indexl + 1)

        prevsize = indexr - indexl + 1

    return inparr[indexl:indexr].base


cpdef double[:] histogram1d(floating[:] data, floating[:] limits, floating delta,
                int bins):

    """
    Calculate the histogram of a 1 dimensional array.

    Inputs:
        data    Input 1 dimensional data, floating
        limits  Lower and upper limits of histogram, floating
        delta   Bin width, floating
        bins    Number of bins in the histogram, int

    Returns:
        hist    Histogram
    """

    cdef int ii, size, index
    cdef str dtype
    cdef double[::1] hist = np.zeros(bins+2)

    size = data.size

    with nogil:
        for ii in range(size):
            if data[ii] < limits[0]:  # Too low, zeroth bin
                index = 0
            elif data[ii] > limits[1]:  # Too high, last bin
                index = bins+1
            else:   # Goldilocks
                index = <int> ((data[ii] - limits[0])/delta) + 1

            hist[index] += 1

    return hist.base  # It's a memory view, so return the actual data
