"""
Module to run the defringing on the baseline data using pyfftw, which is a
lot faster than using numpy or scipy FFT.
"""
import numpy as np
import pyfftw

from scipy.stats import sigmaclip


def defr_base(dat, fouriersigma, npol):
    """
    Given an input array of the shape [ngroup, nchan, npol, 3],
    runs defringing on each polarization for real and imaginary separately,
    returning an array of the same shape.

    Inputs:
        dat           Input data, of shape [ngroup, nchan, npol, 3]
        fouriersigma  Sigma above which to clip in the fourier plane
        npol          Number of polarizations in the data
    Returns:
        outarr        Output data, defringed. Same shape as input
    """

    shape = [dat.shape[0], dat.shape[1]]

    if shape[0] <= 10:
        return None

    defr_dat = np.zeros_like(dat)

    for pp in range(npol):
        adat = pyfftw.zeros_aligned(shape, dtype='complex128')

        adat[:] = np.where(dat[..., pp, 2],
                           dat[..., pp, 0] + 1j * dat[..., pp, 1], 0 + 0j)
        fft = pyfftw.builders.fft2(adat, auto_align_input=True, threads=0)
        fftdat = fft()

        if np.count_nonzero(fftdat) <= 10 or np.count_nonzero(
                np.abs(adat)) <= 10:
            continue

        clipped_fft = sigmaclip(
            np.abs(fftdat), low=fouriersigma, high=fouriersigma)[0]

        if clipped_fft.size == 0:
            return None

        med = np.median(clipped_fft)
        mad = np.median(np.abs(clipped_fft - med))

        fftdat[np.abs(fftdat) > med + fouriersigma * mad] = 0
        ifft = pyfftw.builders.ifft2(fftdat, auto_align_input=True, threads=0)
        ifftdat = ifft()

        defr_dat[..., pp, 0] = ifftdat.real
        defr_dat[..., pp, 1] = ifftdat.imag
        defr_dat[..., pp, 2] = dat[..., pp, 2]

    return defr_dat
