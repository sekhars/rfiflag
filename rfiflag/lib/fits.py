"""
Custom FITS I/O and other manipulation functions.
"""

from astropy.io import fits
from collections import namedtuple
import numpy as np

def fitspars(fitsname):
    """
    Returns the values of the various FITS parameters of a random group FITS
    file. The returned value is a named tuple consisting of the following
    keywords (in order):
        * GCOUNT keyword                (gcount)
        * PCOUNT keyword                (pcount)
        * Number of freq. channels      (nchan)
        * Width of each freq. channel   (chanwidth)
        * Reference frequency           (refreq)
        * GELEMENTS keyword             (gelements)
        * Number of polarizations       (npol)

   fitspars = namedtuple('fitspars', ['gcount', 'pcount', 'nchan',
                                 'chanwidth', 'refreq', 'gelements', 'npol'])

    Inputs:
        fitsname    Name of the input FITS file, str

    Returns:
        fitspars    FITS parameters, named tuple.
    """
    ffile    = fits.open(fitsname, memmap=True)

    gcount   = ffile[0].header['gcount']
    pcount   = ffile[0].header['pcount']
    naxis    = ffile[0].header['naxis']

    # Get the number of channels in a flexible manner
    for ii in range(2, naxis+1):
        key = ffile[0].header['CTYPE'+str(ii)]
        if 'FREQ' in key:
            chanval = ii
            break

    # Get the number of polarizations in a flexible manner
    for ii in range(2, naxis+1):
        key = ffile[0].header['CTYPE'+str(ii)]
        if 'STOKES' in key:
            stokesval = ii
            break

    nchan       = ffile[0].header['naxis' + str(chanval)]
    chanwidth   = ffile[0].header['CDELT' + str(chanval)] # In Hz
    refreq      = ffile[0].header['CRVAL' + str(chanval)]
    refpix      = ffile[0].header['CRPIX' + str(chanval)]
    # We want the frequency at the first pixel
    refreq      = refreq - (refpix - 1)*chanwidth

    # Number of polarizations
    npol        = ffile[0].header['naxis' + str(stokesval)]

    refreq      /= 1E6 # Change to MHz
    chanwidth   /= 1E6 # Change to MHz

    # Loop through the header to find the value of gelements; NAXIS1 always 0
    gelements = 1
    for ii in range(2, naxis):
        gelements *= ffile[0].header['naxis'+str(ii)]

    ffile.close()

    fitspars = namedtuple('fitspars', ['gcount', 'pcount', 'nchan',
                                 'chanwidth', 'refreq', 'gelements', 'npol'])
    retval   = fitspars(gcount, pcount, nchan, chanwidth, refreq,
                                                             gelements, npol)
    return retval


def getfitschunk(gcount):
   """
    Determines a chunk size to use to iterate over the FITS file, given
    the total group count.

    Args:
        gcount      Total number of groups in the FITS file (int)
    Returns:
        chunksize   Chunksize to use (int)
   """

   # Set a maximum chunksize of 10,000 reducing in steps of 1000
   chunksize = 100000
   try:
      while gcount / chunksize < 1:
         chunksize -= 1000
   except ZeroDivisionError:  # gcount is smaller than 1000
      chunksize = 1

   return chunksize


def getuvlim(fitsname):
    """
    Returns the values of the min and max UV values in a FITS file


    fitsname    Name of input FITS file  (string)
    """

    ffile = fits.open(fitsname, memmap=True)

    # Get scaling factors for U, V - Need to divide out to get the
    # UV values in lambda
    uscale   = ffile[0].header['PSCAL1']
    vscale   = ffile[0].header['PSCAL2']
    wscale   = ffile[0].header['PSCAL3']

    gcount   = ffile[0].header['gcount']

    chunksize = getfitschunk(gcount)

    maxu = maxv = maxw = maxuv = 0
    minu = minv = minw = minuv = 0
    # Iterate through FITS file in steps of chunksize
    for group in range(1, gcount, chunksize):
        print("Reading group ", group, "/", gcount, end="\r")

        uval  = ffile[0].data.par(0)[group:group+chunksize]/uscale
        vval  = ffile[0].data.par(1)[group:group+chunksize]/vscale
        wval  = ffile[0].data.par(2)[group:group+chunksize]/wscale

        maxu  = np.maximum(maxu, np.max(uval))
        maxv  = np.maximum(maxv, np.max(vval))
        maxw  = np.maximum(maxw, np.max(wval))
        maxuv = np.maximum(maxuv, np.max(np.sqrt(uval**2, vval**2)))

        minu  = np.minimum(minu, np.min(uval))
        minv  = np.minimum(minv, np.min(vval))
        minw  = np.minimum(minw, np.min(wval))
        minuv = np.minimum(minuv, np.min(np.sqrt(uval**2, vval**2)))

    ffile.close()

    UVLimits = namedtuple('UVLimits', ['umin', 'umax', 'vmin', 'vmax',
                                          'wmin', 'wmax', 'uvmin', 'uvmax'])

    retval = UVLimits(minu, maxu, minv, maxv, minw, maxw, minuv, maxuv)

    return retval

def get_scan_edges(fitsfile, break_width=180):
    """
    Given an input fitsfile, returns two arrays containing the groups
    corresponding to scan edges.  A scan break is defined as a gap in the time
    between two groups greater than break_width (in seconds).

    Inputs:
        fitsfile        Name of input FITS file, str
        break_width     Width of scan break (in seconds), int

    Returns:
        scan_begs       List of groups that are the beginning of scans, ndarray
        scan_ends       List of groups that are the end of scans, ndarray
    """

    pars     = fitspars(fitsfile)
    chunk    = getfitschunk(pars.gcount)

    group_beg = [0,]
    group_end = []

    with fits.open(fitsfile, memmap=True) as ffile:
        for group in range(0, pars.gcount, chunk):
            date = ffile[0].data[group:group+chunk].par('DATE')
            date *= 86400.0 # Number of seconds in a solar day
            date_diff = np.ediff1d(date)

            indices = np.where(date_diff >= break_width)[0]

            group_beg.extend(group + indices + 1)
            group_end.extend(group + indices)

    group_end.append(pars.gcount)
    return np.array(group_beg), np.array(group_end)


