# distutils: language = c++
import numpy as np
cimport numpy as np

from rfiflag.lib.cstats cimport std, median

cimport cython

from libc.math cimport fabs, floor
from libc.stdlib cimport calloc, free
from libcpp.algorithm cimport sort

ctypedef cython.floating flt
ctypedef cython.integral cint

# Define a fused type of only the real numbers, exclude complex
ctypedef fused real_nums:
    cython.int
    cython.long
    cython.float
    cython.double

from cython.parallel import prange, threadid

cdef real_nums _c_min(real_nums a, real_nums b) nogil:
    """
    Returns the minimum of a and b. a and b must be of the same type
    """

    return a if a < b else b

cdef real_nums _c_max(real_nums a, real_nums b) nogil:
    """
    Returns the maximum of a and b. a and b must be of the same type
    """

    return a if a > b else b

def surface_stats(flt [:,:] dat, int halft, int halfc, flt perc):
    """
    Runs Ramana Athreya's surface stats algorithm on the input
    2 dimensional array - This provides sharper cutoffs to identify sporadic
    RFI.

    Inputs:
        dat           Input 2D numpy array
        halft         Half size of the sliding window in the x direction
        halfc         Half size of the sliding window in the y direction
        perc          Subset (percentage) of points to use for calculating statistics

    Returns:
       stdarr         Array containing the local RMS at every point
       rngarr         Array containing the local range at every point
       dvnarr         Array containing the local deviation at every point
    """

    cdef int tt, cc, npt, tid
    cdef int tsize, csize
    cdef int tmin, tmax, cmin, cmax, ii, jj, kk, beg, end
    cdef int count, subnpt, index,indexl
    cdef double rng, refvalue

    cdef double[:,:] dumarr = np.zeros([10,(2*halft+1) * (2*halfc + 1)])
    cdef double[:,:] stdarr = np.zeros_like(dat)
    cdef double[:,:] rngarr = np.zeros_like(dat)
    cdef double[:,:] dvnarr = np.zeros_like(dat)
    #cdef double[:,:] medarr = np.zeros_like(dat)

    shape = dat.shape
    tsize = shape[0]
    csize = shape[1]
    indexl = 1

    npt = (2*halft+1) * (2*halfc+1)

    for tt in prange(tsize, nogil=True, schedule='dynamic'):
        subnpt = <int> floor(perc * npt)
        tid = threadid()

        for cc in range(csize):
            if dat[tt,cc] == 0: # Point is flagged, so skip
                continue

            tmin = 0 if tt < halft else tt-halft
            tmax = 2*halft + 1 if tt < halft else tt + halft

            tmin = tsize - (2*halft + 1) if tt + halft >= tsize else tmin
            tmax = tsize if tt+halft >= tsize else tmax

            cmin = 0 if cc < halfc else cc-halfc
            cmax = 2*halfc + 1 if cc < halfc else cc + halfc

            cmin = csize - (2*halfc + 1) if cc + halfc >= csize else cmin
            cmax = csize if cc+halfc >= csize else cmax

            count = 0
            for ii in range(tmin, tmax):
                for jj in range(cmin, cmax):
                    kk = (tmax-tmin+1)*ii + jj
                    if dat[ii, jj] != 0:
                        dumarr[tid, count] = dat[ii, jj]
                        count = count + 1

            if count < subnpt:
                subnpt = count

            # Sort the array, using cpp sort
            sort(&dumarr[tid, 0], (&dumarr[tid, 0]) + count)

            rng = dumarr[tid, count-1] - dumarr[tid, 0]

            refvalue = dat[tt, cc]

            # Get index of the point in the flattened, sorted array
            index = 0
            while fabs(dumarr[tid, index] - refvalue) > 0:
                index = index + 1

            beg = _c_max(<int> index - subnpt, <int> 0)
            end = _c_min(<int> index, <int> count-subnpt)

            for ii in range(beg, end):
                if rng > dumarr[tid, ii+subnpt] - dumarr[tid, ii]:
                    indexl = ii
                    rng = dumarr[tid, ii+subnpt] - dumarr[tid, ii]
                    # rfiflag.lib.cmath.std is ~ 2.5X faster than Numpy std

            stdarr[tt, cc] = std(dumarr[tid, indexl:indexl+subnpt], subnpt)
            rngarr[tt, cc] = rng
            dvnarr[tt, cc] = refvalue - dumarr[tid, (subnpt+1)/2]
            #medarr[tt, cc] = median(dumarr[tid, indexl:indexl+subnpt], subnpt)

    return stdarr.base, rngarr.base, dvnarr.base
