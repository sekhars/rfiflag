"""
Module to do string manipulations + some simple stuff like clearling the
current line before printing etc.
"""

def clearline(size=80):
   """
   Clears a line that is terminated with a \r character rather than a newline
   so that trailing characters don't hang around the ends of the line

   Inputs:
      size     Length of the line to clear out

   Returns:
      None
   """

   print("", end="\r")

   for nn in range(size):
      print(" ", end="")

   print("", end="\r")


