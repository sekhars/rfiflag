import click

ctx = dict(help_option_names=['-h', '--help'])


@click.group(context_settings=ctx)
def main():
    pass


@main.command(short_help='Flag an entire baseline')
@click.argument('fitsfile', type=click.Path(exists=True))
@click.argument('baseline', type=int)
@click.option(
    '--polarization',
    '-p',
    default=0,
    type=int,
    help='Polarization to flag  [default: 0 (all)]')
def baseline(fitsfile, baseline, polarization):
    """
    Given an input FITSFILE, flags the POLN polarization(s) on BASELINE.
    THe polarization can be specified as follows:

    0 = all polarizations
    1 = first polarization in the file,
    2 = second polarization

    and so on.

    It does not matter whether the polarizations are linear or circular.
    """

    from astropy.io import fits
    from rfiflag.lib import antennas
    from rfiflag.lib import fits as rfits
    import numpy as np

    baseline -= 1

    fitspars = rfits.fitspars(fitsfile)
    chunk = rfits.getfitschunk(fitspars.gcount)

    with fits.open(fitsfile, memmap=True, mode='update') as ffile:
        for group in range(0, fitspars.gcount, chunk):
            print(
                "Processing group {}/{}".format(group, fitspars.gcount),
                end='\r')
            basepar = ffile[0].data[group:group + chunk].par('BASELINE')
            basepar = antennas.baseline_aips_sequential(basepar)

            idx = basepar == baseline

            imgdat = ffile[0].data[group:group + chunk].data

            if polarization == 0:
                imgdat[idx, ..., 2] = 0
            else:
                polarization -= 1
                imgdat[idx, ..., polarization, 2] = 9


@main.command(
    short_help='Run a robust flagging sliding algorithm in the time-frequency'
    ' plane')
@click.argument('fitsfile', type=click.Path(exists=True))
@click.option(
    '--nsigma',
    '-n',
    type=float,
    default=3.0,
    help='Sigma above which to flag RFI [default:3.0]')
@click.option(
    '--fsigma',
    '-f',
    type=float,
    default=2.5,
    help='Sigma above which to clip in the fourier plane [default:2.5]')
@click.option(
    '--halfc',
    type=float,
    default=5,
    help='Half size of window in the frequency axis (in pixels)  [default:5]')
@click.option(
    '--halft',
    type=float,
    default=5,
    help='Half size of window in the time axis (in pixels)  [default:5]')
@click.option(
    '--perc',
    type=float,
    default=30,
    help=
    'Percentage of data to use for calculation of surface stats [default:30]')
def defr_flag_sliding(fitsfile, nsigma, fsigma, halfc, halft, perc):
    """
    Given an input FITSFILE, runs FFT-based defringing before calculating and
    applying flags to the original dataset. The flags are calculated and
    applied on the temporarily defringed data, and the flags are subsequently
    transferred to the original data.

    Statistics are calculated using a sliding window across baselines. The
    cutoffs are calcualted and applied separately for different baselines.

    The --nsigma option determines the level above which RFI is flagged
    per baseline. The --fsigma option determines the sigma used in the
    fourier plane to defringe.
    """

    import numpy as np
    import os
    from astropy.io import fits
    from rfiflag.lib import defringe, strings
    from rfiflag.lib import fits as rfits
    from rfiflag.defr_flag import sliding

    import matplotlib.pyplot as plt

    if perc > 100 or perc < 0:
        msg = 'Input percentage must be between 0 and 100'
        raise ValueError(msg)

    fitspars = rfits.fitspars(fitsfile)
    chunk = rfits.getfitschunk(fitspars.gcount)

    ffile = fits.open(fitsfile, mode='update', memmap=True)

    perc /= 100.  # perc should be <= 1 after this

    strings.clearline()
    max_baseline = sliding.calc_and_write_base_stats(
        ffile, fitspars, chunk, fsigma, halfc, halft, perc)

    strings.clearline()
    cutoffs_std, cutoffs_dvn, cutoffs_rng = sliding.calc_cutoffs(
        max_baseline, nsigma)

    strings.clearline()
    sliding.apply_cutoffs(ffile, fitspars, chunk, cutoffs_std, cutoffs_dvn,
                          cutoffs_rng, max_baseline, fsigma, halfc, halft,
                          perc)

    strings.clearline()
    # Delete all the temporary files
    print("Cleaning up", end="\r")
    for bb in range(max_baseline):
        try:
            os.unlink('tmpstd%03d' % (bb + 1))
        except FileNotFoundError:
            pass

        try:
            os.unlink('tmpdvn%03d' % (bb + 1))
        except FileNotFoundError:
            pass

        try:
            os.unlink('tmprng%03d' % (bb + 1))
        except FileNotFoundError:
            pass

        try:
            os.unlink('tmpmed%03d' % (bb + 1))
        except FileNotFoundError:
            pass

    ffile.close()


@main.command(
    short_help='Run a robust flagging algorithm in the time-frequency'
    ' plane')
@click.argument('fitsfile', type=click.Path(exists=True))
@click.option(
    '--nsigma',
    '-n',
    type=float,
    default=3.0,
    help='Sigma above which to flag RFI [default:3.0]')
@click.option(
    '--fsigma',
    '-f',
    type=float,
    default=2.5,
    help='Sigma above which to clip in the fourier plane [default:2.5]')
def defr_flag_cumul(fitsfile, nsigma, fsigma):
    """
    Given an input FITSFILE, runs FFT-based defringing before calculating and
    applying flags to the original dataset. The flags are calculated and
    applied on the temporarily defringed data, and the flags are subsequently
    transferred to the original data.

    Statistics are accumulated cumululatively over the whole baseline - and
    cutoffs are calculated and applied separately for different baselines.

    The --nsigma option determines the level above which RFI is flagged
    per baseline. The --fsigma option determines the sigma used in the
    fourier plane to defringe.
    """

    import numpy as np
    import os
    from astropy.io import fits
    from rfiflag.lib import defringe, strings, antennas
    from rfiflag.lib import fits as rfits
    from rfiflag.defr_flag import cumul

    import matplotlib.pyplot as plt

    fitspars = rfits.fitspars(fitsfile)
    chunk = rfits.getfitschunk(fitspars.gcount)

    ffile = fits.open(fitsfile, mode='update', memmap=True)

    max_baseline = 0
    # Calculate maximum baseline number
    for group in range(0, fitspars.gcount, chunk):
        print(
            "Checking baselines group {}/{}".format(group, fitspars.gcount),
            end="\r")

        baselines = ffile[0].data[group:group + chunk].par('BASELINE')
        try:  # See if baselines need to be converted to sequential values
            baselines = antennas.baseline_aips_sequential(baselines)
        except:
            pass

        max_baseline = np.maximum(max_baseline, baselines.max())

    strings.clearline()
    cumul.defr_and_write_base(ffile, fitspars, chunk, fsigma, max_baseline)

    strings.clearline()
    cutoffs_ri, cutoffs_amp = cumul.calc_cutoffs(max_baseline, nsigma)

    strings.clearline()
    cumul.apply_cutoffs(ffile, fitspars, chunk, cutoffs_ri, cutoffs_amp,
                        max_baseline, fsigma)

    strings.clearline()
    # Delete all the temporary files
    print("Cleaning up", end="\r")
    for bb in range(max_baseline):
        try:
            os.unlink('tmpri%03d' % (bb + 1))
        except FileNotFoundError:
            pass

        try:
            os.unlink('tmpamp%03d' % (bb + 1))
        except FileNotFoundError:
            pass

    ffile.close()


@main.command(
    short_help='Run the CASA RFlag algorithm in the time-channel plane')
@click.argument('fitsfile', type=click.Path(exists=True))
@click.option(
    '--tsigma',
    '-t',
    type=float,
    default=5.0,
    help='Sigma above which to flag RFI in the time direction [default:5.0]')
@click.option(
    '--csigma',
    '-c',
    type=float,
    default=5.0,
    help='Sigma above which to flag RFI in the channel direction [default:5.0]'
)
@click.option(
    '--fsigma',
    '-f',
    type=float,
    default=2.5,
    help='Sigma above which to clip in the fourier plane [default:2.5]')
@click.option(
    '--twin',
    type=int,
    default=5,
    help='Size of sliding window in time direction in pixels [default: 5]')
@click.option(
    '--defringe',
    'defringe_f',
    is_flag=True,
    help='Defringe data before running algorithm')
def rflag(fitsfile, tsigma, csigma, fsigma, twin, defringe_f):
    """
    Given an input FITSFILE, runs FFT-based defringing before calculating and
    applying flags to the original dataset. The flags are calculated and
    applied on the temporarily defringed data, and the flags are subsequently
    transferred to the original data.

    This command applied the RFlag algorithm as described here
    https://casa.nrao.edu/Release4.1.0/doc/UserMan/UserMansu164.html

    The --tsigma option determines the level above which RFI is flagged
    in the time direction per baseline, and the --csigma option similarly in
    the channel plane. The --fsigma option determines the sigma used in the
    fourier plane to defringe.
    """

    from astropy.io import fits
    import numpy as np
    from rfiflag.lib import fits as rfits
    from rfiflag.lib import antennas, defringe

    from rfiflag.defr_flag import rflag

    fitspars = rfits.fitspars(fitsfile)

    scanbeg, scanend = rfits.get_scan_edges(fitsfile)

    with fits.open(fitsfile, memmap=True, mode='update') as ffile:

        for gbeg, gend in zip(scanbeg, scanend):
            print(
                "Processing group {}/{}".format(gbeg, fitspars.gcount),
                end="\r")

            baselines = ffile[0].data[gbeg:gend + 1].par('BASELINE')

            try:  # See if baselines need to be converted to sequential values
                baselines = antennas.baseline_aips_sequential(baselines)
            except:
                pass

            imgdat = np.squeeze(ffile[0].data[gbeg:gend + 1].data)
            imgdat = imgdat.byteswap().newbyteorder()

            for bb in np.unique(baselines):
                idx = (baselines == bb)
                basedat = imgdat[idx]

                if defringe_f:
                    defrdat = defringe.defr_base(basedat, fsigma,
                                                 fitspars.npol)
                else:
                    defrdat = basedat

                if defrdat is None:
                    continue

                datre = np.where(defrdat[..., 2], defrdat[..., 0], 0)
                datim = np.where(defrdat[..., 2], defrdat[..., 1], 0)

                if np.count_nonzero(datre) < 10 or np.count_nonzero(datim) < 10:
                    continue

                datre = rflag.flagger(datre, tsigma, csigma, fsigma, twin,
                                      fitspars.npol)
                datim = rflag.flagger(datim, tsigma, csigma, fsigma, twin,
                                      fitspars.npol)

                basedat[..., 2] = np.where((datre == 0) | (datim == 0), 0,
                                           basedat[..., 2])

                imgdat[idx] = basedat

            ffile[0].data[gbeg:gend + 1].base['DATA'] = imgdat[:, None, None,
                                                               None, ...]
            ffile.flush()


#@main.command(short_help='Clip on the gridded UV plane')
#@click.argument('fitsfile', type=click.Path(exists=True))
#@click.option(
#    '--uvlim',
#    default=None,
#    help='Comma separated value of UV lengths in lambda')
#@click.option(
#    '--step',
#    '-s',
#    default=None,
#    type=int,
#    help='Step size of the zone in lambda')
#@click.option(
#    '--boundary',
#    '-b',
#    default=None,
#    type=int,
#    help='Boundary of the zone in lambda')
#@click.option(
#    '--gridsize',
#    '-g',
#    default=10,
#    type=int,
#    help='Pixel size in the gridded UV plane in lambda [default:10]')
#def clip_uv(fitsfile, uvlim, step, boundary, gridsize):
#    """
#    Given an input (residual) FITSFILE , the program median filters the
#    gridded UV plane. The UV plane is then flagged in "zones", with each
#    zone being of width --step (specified in units of lambda). The zones
#    will stop at --boundary lambda, woth everything from --boundary lambda
#    upward considered as one single zone.
#
#    Typically --step will be of the order of 20-25% of the maximum UV length,
#    and --boundary will be 60-70% of the maximum UV length. Such a zoning
#    accounts for the fact that visibilities at different UV lengths do
#    not see the same sky.
#
#    If --step and --boundary are not specified, they will be calculated
#    within the program.
#
#    Optionally, the UV range within which to operate can be specified with
#    --uvlim. By default, the entire UV range is used.
#    """
#
#    from rfiflag.lib import antennas, gridder
#    from rfiflag.lib import fits as rfits
#    from astropy.io import fits
#    import numpy as np
#
#    fitspars = rfits.fitspars(fitsfile)
#    chunk = rfits.getfitschunk(fitspars.gcount)
#
#    if uvlim is None:
#        uvlim = rfits.getuvlim(fitsfile)
#        uvmin = uvlim.uvmin
#        uvmax = uvlim.uvmax
#    else:
#        uvlim = sorted([int(uv) for uv in uvlim.split(',')])
#        uvmin = uvlim[0]
#        uvmax = uvlim[1]
#
#    if step is None:
#        step = uvlim.uvmax / 4.
#    else:
#        if step > uvlim.uvmax:
#            step = uvlim.uvmax
#            boundary = uvlim.uvmax
#            print("Specified step size larger than maximum UV length", end=" ")
#            print("Statistics will be calculated over entire UV range.")
#
#    if boundary is None:
#        boundary = uvlim.uvmax / 2.
#
#    freqfactor = np.array(1. + (
#        fitspars.chanwidth * np.arange(fitspars.nchan) / fitspars.refreq))
#
#    uvgrid = gridder.read_and_grid_uv(fitsfile, uvmin, uvmax, fitspars.gcount,
#                                      fitspars.nchan, gridsize, freqfactor)
