from setuptools import setup, find_packages
from setuptools.extension import Extension
from Cython.Build import cythonize
import numpy

import Cython.Compiler.Options
Cython.Compiler.Options.annotate = True

def readme():
    with open('README.md') as f:
        return f.read()

ext_modules = [
        Extension(
            "*", ['rfiflag/lib/*.pyx'],
            extra_compile_args=['-O3', '-fopenmp', '-march=native'],
            extra_link_args=['-fopenmp'],
        )
    ]

setup (
        name='rfiflag',
        version='0.2.dev0',
        packages=find_packages(),
        include_package_data=True,
        license='MIT License',
        long_description=readme(),
        ext_modules=cythonize(ext_modules,
            compiler_directives={'boundscheck':False, 'wraparound':False,
                'embedsignature':True, 'cdivision':True, 'language_level':3},
            annotate=True, language='c++'),
        include_dirs=[numpy.get_include()],
        entry_points="""
            [console_scripts]
            rfiflag=rfiflag.rfiflag:main
            """
        )
